programme :-
    premiere_etape(Tbox,Abi,Abr),
    deuxieme_etape(Abi,Abi1,Tbox),
    troisieme_etape(Abi1,Abr).

/*==================================================================================*/
premiere_etape(Tbox,Abi,Abr) :-
    setof((X,Y), equiv(X,Y),Tboxtemp),
    concept(Tboxtemp),
    pas_autoref(Tboxtemp),
    traitement_Tbox(Tboxtemp,Tbox),
    setof((X,Y),inst(X,Y),Abitemp),
    concept(Abitemp),
    setof((X,Y,Z),instR(X,Y,Z),Abr),
    concept(Abr),
    traitement_Abox(Abitemp,Tbox,Abi).

/*concept*/
concept([]).
concept([L|Ls]) :- concept(L), concept(Ls).
/*Abr*/
concept((X,Y,Z)) :- iname(X), iname(Y), rname(Z).
/*Abi*/
concept((X,Y)) :- iname(X), concept(Y).
/*Tbox*/
concept((X,Y)) :- concept(X), concept(Y).

concept(X) :- cnamena(X).
concept(X) :- cnamea(X).
concept(not(X)) :- concept(X).
concept(and(X,Y)) :- concept(X),concept(Y).
concept(or(X,Y)) :- concept(X),concept(Y).
concept(some(X,Y)) :- rname(X),concept(Y).
concept(all(X,Y)) :- rname(X),concept(Y).


/*pas_autoref*/
pas_autoref([]).
pas_autoref([L|Ls]) :- pas_autoref(L), pas_autoref(Ls), !.

pas_autoref((CC,EC)) :-
    liste_concept_complexe(EC,[],L), !, are_not_member([CC],L), pas_autoref([CC|L],L), !.

pas_autoref(_) :- throw("Erreur, il y a un concept autoréférent").

pas_autoref(_,[]).
pas_autoref(LCC,[L|Ls]) :-
    equiv(L,EC), liste_concept_complexe(EC,[],Lr), !,
    are_not_member(LCC,Lr),
    concat(LCC,Lr,Lt),
    pas_autoref(Lt,Lr),
    pas_autoref(LCC,Ls).

liste_concept_complexe(all(_,C),L,Lp) :- liste_concept_complexe(C,L,Lp).
liste_concept_complexe(some(_,C),L,Lp) :- liste_concept_complexe(C,L,Lp).
liste_concept_complexe(not(X),L,Lp) :- liste_concept_complexe(X,L,Lp).
liste_concept_complexe(and(C1,C2),L,Lpp) :- liste_concept_complexe(C1,L,Lp), liste_concept_complexe(C2,Lp,Lpp).
liste_concept_complexe(or(C1,C2),L,Lpp) :- liste_concept_complexe(C1,L,Lp), liste_concept_complexe(C2,Lp,Lpp).

liste_concept_complexe(C,L,[C|L]) :- cnamena(C), are_not_member([C],L).
liste_concept_complexe(_,L,L).

are_not_member([],_).
are_not_member([L|Ls],L2) :- not(member(L,L2)), are_not_member(Ls,L2).

/*traitement_Tbox*/
traitement_Tbox([],[]).
traitement_Tbox([(X,Y)|Es],[(X,Ypp)|Ss]) :- remplacement_Tbox(Y,Yp), nnf(Yp,Ypp), traitement_Tbox(Es,Ss).

/*traitement_Abox*/
traitement_Abox([],_,[]).
traitement_Abox([(X,Y)|Es],Tbox,[(X,Ypp)|Ss]) :- remplacement_Abox(Y,Yp,Tbox), nnf(Yp,Ypp), traitement_Abox(Es,Tbox,Ss).

/*==================================================================================*/
deuxieme_etape(Abi,Abi1,Tbox) :- 
    saisie_et_traitement_prop_a_demontrer(Abi,Abi1,Tbox).

saisie_et_traitement_prop_a_demontrer(Abi,Abi1,Tbox) :-
    nl, write('Entrez le numero du type de proposition que vous voulez demontrer :'),
    nl, write('1 Une instance donnee appartient a un concept donne.'),
    nl, write('2 Deux concepts n"ont pas d"elements en commun(ils ont une intersection vide).'),
    nl, read(R), suite(R,Abi,Abi1,Tbox).

suite(1,Abi,Abi1,Tbox) :-
    acquisition_prop_type1(Abi,Abi1,Tbox),!.
suite(2,Abi,Abi1,Tbox) :-
    acquisition_prop_type2(Abi,Abi1,Tbox),!.
suite(_,Abi,Abi1,Tbox) :-
    nl, write('Cette reponse est incorrecte.'),
    nl, saisie_et_traitement_prop_a_demontrer(Abi,Abi1,Tbox).

acquisition_prop_type1(Abi,Abi1,Tbox):-
    nl, write("Proposition (I,C): "),
    nl, read(R), traitement_prop_type1(R,Abi,Abi1,Tbox).
traitement_prop_type1((RI,RC),Abi,[(RI,Cp)|Abi],Tbox) :-
    iname(RI), concept(not(RC)), remplacement_Abox(not(RC),C,Tbox), nnf(C,Cp).
traitement_prop_type1(_,Abi,Abi1,Tbox) :-
    nl, write('Proposition incorrecte.'),
    acquisition_prop_type1(Abi,Abi1,Tbox).

acquisition_prop_type2(Abi,Abi1,Tbox):-
    nl, write("Proposition (C1,C2): "),
    nl, read(R), traitement_prop_type2(R,Abi,Abi1,Tbox).
traitement_prop_type2((C1,C2),Abi,[(inst,Cp)|Abi],Tbox) :-
	concept(and(C1,C2)), remplacement_Abox(and(C1,C2),C,Tbox), nnf(C,Cp).
traitement_prop_type2(_,Abi,Abi1,Tbox) :-
    nl, write('Proposition incorrecte.'),
    acquisition_prop_type2(Abi,Abi1,Tbox).

/*==================================================================================*/
troisieme_etape(Abi,Abr) :-
    tri_Abox(Abi,Lie,Lpt,Li,Lu,Ls), !,
    nl,write('Abox initial + proposition:'),
    affiche_evolution_Abox(Lie, Lpt, Li, Lu, Ls, Abr, Lie, Lpt, Li, Lu, Ls, Abr),
	resolution(Lie,Lpt,Li,Lu,Ls,Abr),
	nl,write('Youpiiiiii, on a demontre la proposition initiale !!!').

tri_Abox([],[],[],[],[],[]).
tri_Abox([(I,some(R,C))|Abis],[(I,some(R,C))|Lies],Lpt,Li,Lu,Ls) :- tri_Abox(Abis,Lies,Lpt,Li,Lu,Ls).
tri_Abox([(I,all(R,C))|Abis],Lie,[(I,all(R,C))|Lpts],Li,Lu,Ls) :- tri_Abox(Abis,Lie,Lpts,Li,Lu,Ls).
tri_Abox([(I,and(C1,C2))|Abis],Lie,Lpt,[(I,and(C1,C2))|Lis],Lu,Ls) :- tri_Abox(Abis,Lie,Lpt,Lis,Lu,Ls).
tri_Abox([(I,or(C1,C2))|Abis],Lie,Lpt,Li,[(I,or(C1,C2))|Lus],Ls) :- tri_Abox(Abis,Lie,Lpt,Li,Lus,Ls).
tri_Abox([E|Abis],Lie,Lpt,Li,Lu,[E|Lss]) :- tri_Abox(Abis,Lie,Lpt,Li,Lu,Lss).

resolution(_,_,_,_,Ls,_) :- test_clash(Ls).
resolution(Lie,Lpt,Li,Lu,Ls,Abr) :- complete_some(Lie,Lpt,Li,Lu,Ls,Abr),!.
resolution(Lie,Lpt,Li,Lu,Ls,Abr) :- transformation_and(Lie,Lpt,Li,Lu,Ls,Abr),!.
resolution(Lie,Lpt,Li,Lu,Ls,Abr) :- deduction_all(Lie,Lpt,Li,Lu,Ls,Abr),!.
resolution(Lie,Lpt,Li,Lu,Ls,Abr) :- transformation_or(Lie,Lpt,Li,Lu,Ls,Abr),!.

complete_some([(A,some(R,C))|Lies],Lpt,Li,Lu,Ls,Abr) :-
    nl,write('================================='),
    nl,write('Règle ∃:'),
    genere(B),
    evolue((B,C), Lies, Lpt, Li, Lu, Ls, Lie1, Lpt1, Li1, Lu1, Ls1),
    affiche_evolution_Abox([(A,some(R,C))|Lies],Lpt,Li,Lu,Ls,Abr,Lie1,Lpt1,Li1,Lu1,Ls1,[(A,B,R)|Abr]),!,
    resolution(Lie1,Lpt1,Li1,Lu1,Ls1,[(A,B,R)|Abr]).

transformation_and(Lie, Lpt, [(A,and(C,D))|Lis], Lu, Ls, Abr) :-
    nl,write('================================='),
    nl,write('Règle ⊓:'),
    evolue([(A,C),(A,D)], Lie, Lpt, Lis, Lu, Ls, Lie1, Lpt1, Li1, Lu1, Ls1),
    affiche_evolution_Abox(Lie,Lpt,[(A,and(C,D))|Lis],Lu,Ls,Abr,Lie1,Lpt1,Li1,Lu1,Ls1,Abr),!,
    resolution(Lie1,Lpt1,Li1,Lu1,Ls1,Abr).

deduction_all(Lie, [(A,all(R,C))|Lpts], Li, Lu, Ls, Abr) :- 
    nl,write('================================='),
    nl,write('Règle ∀:'),
    apply_all(A,R,C,Abr,Res),
    evolue(Res, Lie, Lpts, Li, Lu, Ls, Lie1, Lpt1, Li1, Lu1, Ls1),
    affiche_evolution_Abox(Lie,[(A,all(R,C))|Lpts],Li,Lu,Ls,Abr,Lie1,Lpt1,Li1,Lu1,Ls1,Abr),!,
    resolution(Lie1,Lpt1,Li1,Lu1,Ls1,Abr).

apply_all(_,_,_,[],[]).
apply_all(A,R,C,[(A,B,R)|Abrs],[(B,C)|Res]) :- apply_all(A,R,C,Abrs,Res).
apply_all(A,R,C,[_|Abrs],Res) :- apply_all(A,R,C,Abrs,Res).

transformation_or(Lie, Lpt, Li, [(A,or(C,D))|Lus], Ls, Abr) :-
    nl,write('================================='),
    nl,write('Règle ⊔ (tableau gauche):'),
    evolue((A,C), Lie, Lpt, Li, Lus, Ls, Lie1, Lpt1, Li1, Lu1, Ls1),
    affiche_evolution_Abox(Lie,Lpt,Li,[(A,or(C,D))|Lus],Ls,Abr,Lie1,Lpt1,Li1,Lu1,Ls1,Abr),!,
    resolution(Lie1,Lpt1,Li1,Lu1,Ls1,Abr),
    nl,write('================================='),
    nl,write('Règle ⊔ (tableau droit):'),
    evolue((A,D), Lie, Lpt, Li, Lus, Ls, Lie2, Lpt2, Li2, Lu2, Ls2),
    affiche_evolution_Abox(Lie,Lpt,Li,[(A,or(C,D))|Lus],Ls,Abr,Lie2,Lpt2,Li2,Lu2,Ls2,Abr),!,
    resolution(Lie2,Lpt2,Li2,Lu2,Ls2,Abr).

evolue([], Lie, Lpt, Li, Lu, Ls, Lie, Lpt, Li, Lu, Ls).
evolue([E|Es], Lie, Lpt, Li, Lu, Ls, Lie2, Lpt2, Li2, Lu2, Ls2) :-
    evolue(E, Lie, Lpt, Li, Lu, Ls, Lie1, Lpt1, Li1, Lu1, Ls1),
    evolue(Es, Lie1, Lpt1, Li1, Lu1, Ls1, Lie2, Lpt2, Li2, Lu2, Ls2).

evolue((I,some(R,C)), Lie, Lpt, Li, Lu, Ls, [(I,some(R,C))|Lie], Lpt, Li, Lu, Ls) :- not(member((I,some(R,C)),Lie)).
evolue((I,all(R,C)), Lie, Lpt, Li, Lu, Ls, Lie, [(I,all(R,C))|Lpt], Li, Lu, Ls) :- not(member((I,all(R,C)),Lpt)).
evolue((I,and(C1,C2)), Lie, Lpt, Li, Lu, Ls, Lie, Lpt, [(I,and(C1,C2))|Li], Lu, Ls) :- not(member((I,and(C1,C2)),Li)).
evolue((I,or(C1,C2)), Lie, Lpt, Li, Lu, Ls, Lie, Lpt, Li, [(I,or(C1,C2))|Lu], Ls) :- not(member((I,or(C1,C2)),Lu)).
evolue((I,C), Lie, Lpt, Li, Lu, Ls, Lie, Lpt, Li, Lu, [(I,C)|Ls]) :- not(member((I,C),Ls)).
evolue(_, Lie, Lpt, Li, Lu, Ls, Lie, Lpt, Li, Lu, Ls).

affiche_evolution_Abox(Lie1, Lpt1, Li1, Lu1, Ls1, Abr1, Lie2, Lpt2, Li2, Lu2, Ls2, Abr2) :-
    diff_av_ap(Lie1,Lie2),
    diff_av_ap(Lpt1,Lpt2),
    diff_av_ap(Li1,Li2),
    diff_av_ap(Lu1,Lu2),
    diff_av_ap(Ls1,Ls2),
    diff_av_ap(Abr1,Abr2).

diff_av_ap(Lav,Lap) :- affiche_elem_sup(Lav,Lap), affiche_Abox(Lav,Lap).

affiche_elem_sup([],_).
affiche_elem_sup([L|Lav],Lap) :- not(member(L,Lap)), to_string(L,X), nl, write("- "), write(X), affiche_elem_sup(Lav,Lap),!.
affiche_elem_sup([_|Lav],Lap) :- affiche_elem_sup(Lav,Lap).

affiche_Abox(_,[]).
affiche_Abox(Lav,[L|Lap]) :- not(member(L,Lav)), to_string(L,X), nl, write("+ "), write(X), affiche_Abox(Lav,Lap),!.
affiche_Abox(Lav,[L|Lap]) :- member(L,Lav), to_string(L,X), nl, writef('  %w',[X]), affiche_Abox(Lav,Lap),!.

to_string((A,B,R),Z) :- format(atom(Z),'<~a,~a>:~a',[A,B,R]),!.
to_string((I,C),Z) :- to_string(C,Y), format(atom(Z),'~a:~a',[I,Y]),!.
to_string(not(X),Z) :- to_string(X,Y), format(atom(Z),'¬~a',[Y]),!.
to_string(some(R,C),Z) :- to_string(C,X), format(atom(Z),'∃~a.~a',[R,X]),!.
to_string(all(R,C),Z) :- to_string(C,X), format(atom(Z),'∀~a.~a',[R,X]),!.
to_string(and(C1,C2),Z) :- to_string(C1,X), to_string(C2,Y), format(atom(Z),'(~a) ⊓ (~a)',[X,Y]),!.
to_string(or(C1,C2),Z) :- to_string(C1,X), to_string(C2,Y), format(atom(Z),'(~a) ⊔ (~a)',[X,Y]),!.
to_string(X,X).

test_clash([(A,C)|Ls]) :- member((A,not(C)),Ls), nl, writef('Clash entre %w:%w et %w:¬%w',[A,C,A,C]).
test_clash([(A,not(C))|Ls]) :- member((A,C),Ls), nl, writef('Clash entre %w:¬%w et %w:%w',[A,C,A,C]).
test_clash([_|Ls]) :- test_clash(Ls).

/*remplacement Tbox=================================================================*/
remplacement_Tbox(not(X),not(Y)) :- remplacement_Tbox(X,Y).
remplacement_Tbox(and(X,Y),and(Xp,Yp)) :- remplacement_Tbox(X,Xp), remplacement_Tbox(Y,Yp).
remplacement_Tbox(or(X,Y),or(Xp,Yp)) :- remplacement_Tbox(X,Xp), remplacement_Tbox(Y,Yp).
remplacement_Tbox(some(X,Y),some(X,Yp)) :- remplacement_Tbox(Y,Yp).
remplacement_Tbox(all(X,Y),all(X,Yp)) :- remplacement_Tbox(Y,Yp).
remplacement_Tbox(X,Z) :- cnamena(X), equiv(X,Y), remplacement_Tbox(Y,Z),!.
remplacement_Tbox(X,X) :- cnamea(X),!.


/*remplacement Abox=================================================================*/
remplacement_Abox(not(X),not(Y),Tbox) :- remplacement_Abox(X,Y,Tbox).
remplacement_Abox(and(X,Y),and(Xp,Yp),Tbox) :- remplacement_Abox(X,Xp,Tbox), remplacement_Abox(Y,Yp,Tbox).
remplacement_Abox(or(X,Y),or(Xp,Yp),Tbox) :- remplacement_Abox(X,Xp,Tbox), remplacement_Abox(Y,Yp,Tbox).
remplacement_Abox(some(X,Y),some(X,Yp),Tbox) :- remplacement_Abox(Y,Yp,Tbox).
remplacement_Abox(all(X,Y),all(X,Yp),Tbox) :- remplacement_Abox(Y,Yp,Tbox).
remplacement_Abox(X,Y,Tbox) :- cnamena(X), member((X,Y),Tbox),!.
remplacement_Abox(X,X,_) :- cnamea(X),!.


/*forme normale négative============================================================*/
nnf(not(and(C1,C2)),or(NC1,NC2)):- nnf(not(C1),NC1),
nnf(not(C2),NC2),!.
nnf(not(or(C1,C2)),and(NC1,NC2)):- nnf(not(C1),NC1),
nnf(not(C2),NC2),!.
nnf(not(all(R,C)),some(R,NC)):- nnf(not(C),NC),!.
nnf(not(some(R,C)),all(R,NC)):- nnf(not(C),NC),!.
nnf(not(not(X)),X):-!.
nnf(not(X),not(X)):-!.
nnf(and(C1,C2),and(NC1,NC2)):- nnf(C1,NC1),nnf(C2,NC2),!.
nnf(or(C1,C2),or(NC1,NC2)):- nnf(C1,NC1), nnf(C2,NC2),!.
nnf(some(R,C),some(R,NC)):- nnf(C,NC),!.
nnf(all(R,C),all(R,NC)) :- nnf(C,NC),!.
nnf(X,X).


/*genere============================================================================*/
compteur(1).
genere(Nom) :- 
    compteur(V),nombre(V,L1),
	concat([105,110,115,116],L1,L2),
	V1 is V+1,
	dynamic(compteur/1),
    retract(compteur(V)),
    dynamic(compteur/1),
    assert(compteur(V1)),
	name(Nom,L2),!.
nombre(0,[]).
nombre(X,L1) :-
    R is (X mod 10),
    Q is ((X-R)//10),
    chiffre_car(R,R1),
    char_code(R1,R2),
    nombre(Q,L),
    concat(L,[R2],L1).
chiffre_car(0,'0').
chiffre_car(1,'1').
chiffre_car(2,'2').
chiffre_car(3,'3').
chiffre_car(4,'4').
chiffre_car(5,'5').
chiffre_car(6,'6').
chiffre_car(7,'7').
chiffre_car(8,'8').
chiffre_car(9,'9').


/*concat============================================================================*/
concat([],L1,L1).
concat([X|Y],L1,[X|L2]) :- concat(Y,L1,L2).