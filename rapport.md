# Introduction

## Choix d'implémentation
#### Prise en charge d'une liste plutot que d'une entrée simple
Plusieurs prédicats ont une gestion de liste en entrée pour pouvoir utiliser facilement le prédicat sur un ensemble d'élément, comme pour `concept` et `pas_autoref`.


#### Ordre de résolution et clash
Lorsqu'une une règle ⊔ est appliquée, on commence par résoudre la branche de gauche, s'il y a un clash on passe à celle de droite et on reprend où on en était, sinon le programme s'arrête et retourne **`false`**.


#### Affichage des régles utilisées et des modification de la Abox
La première chose affichée est la Abox étendu puis à chaque étape on affiche la règle utilisé, et la Abox modifiée avec un **+** si une assertion est ajoutée et un **-** pour une assertion rétiré.
On retire chaque assertion utilisé avec une des régles.
Les régles utilisées pour la résolution du problème sont affichées directement par le prédicat qui applique cette règle.
S'il y a un clash, il est affiché avec les assertions concernées.


## Comment utiliser le démonstrateur
Pour utiliser le démonstrateur on lance `programme.` puis, on choisit un type de proposition et enfin, on entre une proposition, les deux éléments séparés par une virgule.
Pour $I$, on entre un nom d'instance et pour $C$, on entre un concept.


#### programme
Le predicat `programme` permet de lancer les 3 étapes du démonstrateur, les 3 prédicats associés sont `premiere_etape`, `deuxieme_etape` et `troisieme_etape`.
Ces prédicats seront détaillés dans les parties 1, 2 et 3 de ce rapport.


# Partie 1
Dans cette partie, on récupère la Abox et la Tbox défini dans un autres fichier à l'aide des prédicats `equiv`, `cnamea`, `cnamena`, `iname`, `rname`, `inst` et `instR`.


## premiere_etape
Le prédicat `premiere_etape` permet:
- la récupération de la Abox et de la Tbox à l'aide du prédicat intégré `setof`
- de vérifier si les leurs éléments sont des concepts à l'aide du prédicat `concept`
- puis de vérifier que la Tbox n'est pas autoréférente à l'aide du prédicat `pas_autoref` 
- enfin de les mettres en forme à l'aide des prédicats `traitement_Tbox` et `traitement_Abox`.


## pas_autoref
Le prédicat `pas_autoref`, vérifie si un concept complexe est autoréférent et révoie une erreur si c'est le cas.
`pas_autoref` utilise `liste_concept_complexe` pour récuperer les concepts complexes qui apparaissent dans une expression complexe.
On vérifie ensuite avec `are_not_member` que la liste des concepts complexes deja parcouru n'est pas dans la liste des concepts complexes.
Puis on vérifie récursivement chaque concept complexe, en gardant dans une liste les concept déjà rencontré.


#### liste_concept_complexe
`liste_concept_complexe` est un prédicat qui va récursivement décomposé et ajouté à une liste, les concepts complexes d'une expression complexe.
Chaque concept est ajouté une seul fois dans la liste.


#### are_not_member
`are_not_member` permet de vérifier qu'aucun des élément d'une liste n'est dans une autre liste.


## concept
Le prédicat `concept` à 2 clauses pour la Abox et 1 clauses pour la Tbox.
Pour les rôles definis dans la Abox, on a une entrée de la forme $(X,Y,Z)$, et on verifie que $X$ et $Y$ sont des noms d'instances et $Z$ est un concept.
Pour les instances de la Abox, l'entrée est de la forme $(X,Y)$ et on vérifie si $X$ est un nom d'instance et $Y$ un concept.
Pour la Tbox, l'entrée est de la forme $(X,Y)$ et on vérifie si $X$ et $Y$ sont des concepts.

Le prédicat `concept` à également une partie récursive qui permet de vérifier si une expression est un concept composée en décomposant l'expression et en vérifiant si les éléments sont des concepts complexes, des concepts atomiques ou des concepts composées.

La correction sémantique se fait à l'aide de la vérification des noms d'instances, de rôles et des concepts.
La correction syntaxique se fait au niveau de la partie récursive, en décomposant, s'il y a une erreur syntaxique le programme retournera **`false`** au lancement.


## traitement_Tbox
Le prédicat `traitement_Tbox` remplace récursivement les concepts complexes présent dans les expressions complexes par leur expression dans la Tbox avec `remplacement_Tbox` pour obtenir des expressions avec uniquement des concepts atomiques.
Puis, la nouvelle expression est mise sous forme normale négative.


## traitement_Abox
Le prédicat `traitement_Abox` remplace récursivement les concepts complexes par leur expression dans la Tbox avec `remplacement_Abox` pour obtenir des expressions avec uniquement des concepts atomiques.
Puis, la nouvelle expression est mise sous forme normale négative.

# Partie 2
Dans cette partie, on récupére la proposition de l'utilisateur, elle peut être de 2 formes, "une instance $I$ appartient au concept $C$" ou "deux concept $C1$ et $C2$ n'ont pas d'élément en commun".

## deuxieme_etape
Le prédicat `deuxieme_etape` permet de lancé `saisie_et_traitement_prop_a_demontrer`, ce qui permet de choisir entre 2 types de propositions puis de retourner une Abox étendue avec la proposition.

## acquisition_prop_type1
Le prédicat `acquisition_prop_type1` est appelé si l'utilisateur choisi une proposition de type "une instance $I$ appartient au concept $C$".
Ici, l'utilisateur doit écrire une proposition avec un nom d'instance et un concept valide, sinon il devra recommencé jusqu'à avoir une proposition valide.
La validité est vérifié dans `traitement_prop_type1`, on vérifie que I est un nom d'instance et C un concept.
Si la proposition passe la vérification alors on applique `remplacement_Abox` et `nnf`.

## acquisition_prop_type2
Le prédicat `acquisition_prop_type2` est appelé si l'utilisateur choisi une proposition de type "deux concept $C1$ et $C2$ n'ont pas d'élément en commun".
Ici, l'utilisateur doit écrire une proposition avec deux concepts valides, sinon il devra recommencé jusqu'à avoir une proposition valide.
La validité est vérifié dans `traitement_prop_type2`, on vérifie que (C1 ⊓ C2) est un concept.
Si la proposition passe la vérification alors on applique `remplacement_Abox` et `nnf`.

# Partie 3
Dans cette partie, on démontre la proposition à l'aide des assertion d'instance, de rôle et l'assertion ajouté par l'utilisateur en appliquant les règles ∃, ⊓, ∀ et ⊔.

## troisieme_etape
Le prédicat `troisieme_etape`, permet de trier la Abox à l'aide de `tri_Abox` en séparant les types d'assertions dans différente liste, pour facilité le processus de résolution, puis affiche la Abox avec `affiche_evolution_Abox` et enfin commence le processus de résolution avec `resolution`.

  
## tri_Abox
Le prédicat `tri_Abox`, sépare les assertions en fonction de leur forme:
- la liste Lie des assertions du type (I,some(R,C))
- la liste Lpt des assertions du type (I,all(R,C))
- la liste Li des assertions du type (I,and(C1,C2))
- la liste Lu des assertions du type (I,or(C1,C2))
- la liste Ls des assertions restantes, à savoir les assertions du type (I,C) ou (I,not(C)), C étant un concept atomique.

## resolution
Le prédicat `résolution`, avant d'appliquer une règle, va faire un test de clash avec `test_clash` sur la liste Ls.
Si le test de clash réussi on ferme la branche et on reprend sur une autre, s'il n'y a plus de branche alors on a réussi la démonstration.
Si le test de clash échoue et qu'on ne peut plus appliquer aucune règle la démonstration échoue et renvoie **`false`**
L'application des règle se fait dans l'ordre (∃, ⊓, ∀, ⊔) et on affiche quelle règle est appliqué chaque étape.

#### test_clash
Le prédicat `test_clash` réussi s'il trouve deux assertions de la forme `a : C` et `a : ¬C` avec C un concept atomique.

## complete_some
Le prédicat `complete_some` correspond à la règle ∃, elle est appliqué si la liste Lie n'est pas vide.
Les assertion de la liste Lie sont de la forme `a : ∃R.C`, on doit ajouté `〈a, b〉: R` à la liste Abr et on utilise `evolue` pour mettre `b : C` dans la bonne liste.
Donc on a récupère a, R et C et on a génére b avec `genere`.
Une fois que la Abox a était modifiée, on affiche les modifications de la Abox avec `affiche_evolution_Abox` puis on passe à une nouvelle étape de résolution.

## transformation_and
Le prédicat `transformation_and` correspond à la règle ⊓, elle est appliqué si la liste Li n'est pas vide.
Les assertion de la liste Li sont de la forme `a : C ⊓ D`, on doit ajouté `a : C` et `a : D` dans la bonne liste à l'aide de `evolue`.
Une fois que la Abox a était modifiée, on affiche les modifications de la Abox avec `affiche_evolution_Abox` puis on passe à une nouvelle étape de résolution.

## deduction_all
Le prédicat `deduction_all` correspond à la règle ∀, elle est appliqué si la liste Lpt n'est pas vide.
Les assertion de la liste Lpt sont de la forme `a : ∀R.C`, pour chaque `〈a, b〉: R` dans Abr, on doit ajouté les assertions `b : C` dans la bonne liste à l'aide de `evolue`.
On ajoute toutes les assertions `b : C` pour pouvoir supprimer l'assertions `a : ∀R.C`.
Une fois que la Abox a était modifiée, on affiche les modifications de la Abox avec `affiche_evolution_Abox` puis on passe à une nouvelle étape de résolution.

## transformation_or
Le prédicat `transformation_or` correspond à la règle ⊔, elle est appliqué si la liste Lu n'est pas vide.
Les assertion de la liste Lu sont de la forme `a : C ⊔ D`, on doit ajouté `a : C` et `a : D` dans 2 copie différente de la bonne liste à l'aide de `evolue` pour généré 2 Abox différentes.
Une fois que la première Abox a était modifiée, on affiche les modifications de la Abox avec `affiche_evolution_Abox` puis on passe à une nouvelle étape de résolution.
S'il y a un clash dans la première Abox alors, on passe à la deuxième Abox et suit le même procédé.

## evolue
Le prédicat `evolue`, va ajouté dans la bonne liste l'élément qui lui est donné si cette élément n'est pas déjà dans la liste.

## affiche_evolution_Abox
Le prédicat `affiche_evolution_Abox`, utilise `diff_av_ap` sur chaque liste et leur nouvelle version pour afficher les éléments supprimés en utilisant `affiche_elem_sup` et les éléments ajoutés et ceux inchangés avec `affiche_Abox`.
Les éléments sont mis dans une forme facilement lisible à l'aide de `to_string`.

#### to_string
Le prédicat `to_string` permet de transformer récursivement les assertions en chaine de caractère en utilisant `format`.


# Autre prédicat utilisé

## remplacement_Tbox
Le prédicat `remplacement_Tbox`, remplace récursivement les concept complexe dans les expressions par leur equivalent en utilisant `equiv` et en appliquant le remplacement sur la nouvelle expression.

## remplacement_Abox
Le prédicat `remplacement_Abox`, remplace récursivement les concept complexe dans les expressions par leur equivalent dans la Tbox en utilsant `member`.

## Annexe
Les prédicats `genere`, `nnf` et `concat` sont donnés dans le sujet.
`genere` permet de généré un nouvel identificateur d'instance.
`nnf` permet de mettre une expression sous forme normale négative.
`concat` permet de concaténer 2 listes.